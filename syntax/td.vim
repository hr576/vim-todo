syntax match DiffAdd /\[[^ ].*\]/
syntax match NonText /\[\ \]/
syntax match Keyword /\*\W/
syntax match Title /\(^\*\W\)\@<=.*/